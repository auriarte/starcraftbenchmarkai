#include "BenchmarkModule.h"
#include <fstream>
#include "RPMCounter.h"
#include "Statistic.h"

std::ofstream fileLog;
#define LOG(Message) fileLog << Message << std::endl

using namespace BWAPI;

bool isMapOnlyMicro = true;

int zealotSpotted = 0; // in scenario RC3 saves the frame when the first Zealot is spotted
double selfKilled = 0;
double enemyKilled = 0;
int refineryDestroyed = 0; // frame when the refinery was destroyed
int refineryRestored = 0; // frame when the refinery was rebuild
double lowerBound;
double upperBound;

RPMCounter rpmCounter;
Statistic avgMPM;
Statistic avgGPM;
static const int RPM_INTERVAL = 1000; // in ms
int lastRPMUpdate = 0;
bool firstGasGathered = false;

bool supplyBlocked = false;
int lastSupplyBocked = 0;
Statistic supplyBlockedStat;
Statistic cushionFactor;

std::map<BWAPI::Unit, int> trainingBuildingLastIdle;
Statistic buildingIdle;

void BenchmarkAI::onStart()
{
	fileLog.open("bwapi-data\\logs\\Benchmark.txt", std::ios_base::app); //append the output
	// Set the command optimization level (reduces high APM, size of bloated replays, etc)
	Broodwar->setCommandOptimizationLevel(MINIMUM_COMMAND_OPTIMIZATION);
	Broodwar->setLocalSpeed(0);

	// only activate complete map information if we are in a melee map
	for (auto& unit : Broodwar->self()->getUnits()) {
		if (unit->getType().isResourceDepot()) {
			isMapOnlyMicro = false;
			break;
		}
	}

	if (isMapOnlyMicro) {
		Broodwar->enableFlag(Flag::CompleteMapInformation);
	} else {
		rpmCounter.init();
	}
}

void BenchmarkAI::onEnd(bool isWinner)
{
	// Calculate metric for RC3
	if (Broodwar->mapName() == "RC3") {
		double timeNormalized = double(Broodwar->getFrameCount() - zealotSpotted) / 4807;
		LOG("Map: " << Broodwar->mapFileName() << " Metric2: " << timeNormalized);
	} else if (Broodwar->mapName() == "T1") {
		double timeNormalized = double(Broodwar->getFrameCount()) / 963;
		LOG("Map: " << Broodwar->mapFileName() << " Metric2: " << 1 - timeNormalized);
	} else if (Broodwar->mapName() == "S1") {
		LOG("Map: " << Broodwar->mapFileName() << " Metric3: " << (enemyKilled / 4) - (selfKilled / 25) );
	} else if (Broodwar->mapName() == "S2-D") {
		int timeElapsedToRecovery = ((refineryRestored == 0) ? Broodwar->getFrameCount() : refineryRestored) - refineryDestroyed;
		double timeNormalized = double(timeElapsedToRecovery) / 3162;
		LOG("Map: " << Broodwar->mapFileName() << " Metric4: " << 1 - timeNormalized);
	} else if (Broodwar->mapName().find("S3") != std::string::npos) {
		LOG("Map: " << Broodwar->mapFileName() << " Macro: " << avgMPM.getMean() << " " << avgGPM.getMean() << " " << supplyBlockedStat.getTotal() << " " << cushionFactor.getMean() << " " << buildingIdle.getTotal());
	} else {
		// Calculate metric 1
		double selfHP = 0;
		for (auto& unit : Broodwar->self()->getUnits()) selfHP += sqrt(unit->getHitPoints() + unit->getShields());
		double enemyHP = 0;
		for (auto& unit : Broodwar->enemy()->getUnits()) enemyHP += sqrt(unit->getHitPoints() + unit->getShields());
		double metric1 = (selfHP - enemyHP) / (double)Broodwar->getFrameCount();
		double normalized = (metric1 < 0) ? metric1 / lowerBound : metric1 / upperBound;
		LOG("Map: " << Broodwar->mapFileName() << " Metric1: " << normalized);
	}

	fileLog.close();
}

void BenchmarkAI::onFrame()
{
	// If the elapsed game time has exceeded 20 minutes
	if (Broodwar->elapsedTime() > 20 * 60) {
		Broodwar->sendText("DRAW");
		Broodwar->leaveGame();
	}

	// Calculate the upper/lower bounds of melee maps
	if (Broodwar->getFrameCount() == 1) {
		// lower bound is when player1 is defeated with the minimum time required and without dealing damage to player 2
		double enemyTotalHP = getTotalSqrtHP(Broodwar->enemy()->getUnits());
		double timeToKillPlayer1 = getTimeToKill(Broodwar->enemy()->getUnits(), Broodwar->self()->getUnits());
		lowerBound = enemyTotalHP / timeToKillPlayer1;

		// upper bound is the opposite
		double selfTotalHP = getTotalSqrtHP(Broodwar->enemy()->getUnits());
		double timeToKillPlayer2 = getTimeToKill(Broodwar->self()->getUnits(), Broodwar->enemy()->getUnits());
		upperBound = selfTotalHP / timeToKillPlayer2;
	}

	if (!isMapOnlyMicro) {
		// update resources per minute stats
		rpmCounter.update();
		int gameMs = Broodwar->getFrameCount() * 42;
		if (gameMs - lastRPMUpdate > RPM_INTERVAL) {
			avgMPM.add(rpmCounter.mpm());
			if (rpmCounter.gpm() > 0 || firstGasGathered) {
				avgGPM.add(rpmCounter.gpm());
				firstGasGathered = true;
			}
			lastRPMUpdate = gameMs;
		}

		// check supply depot blocked
		if (!supplyBlocked) {
			if (Broodwar->self()->supplyTotal() == Broodwar->self()->supplyUsed() && Broodwar->self()->supplyTotal() != 400) {
				supplyBlocked = true;
				lastSupplyBocked = Broodwar->getFrameCount();
			}
		} else {
			if (Broodwar->self()->supplyTotal() != Broodwar->self()->supplyUsed()) {
				supplyBlocked = false;
				supplyBlockedStat.add(Broodwar->getFrameCount() - lastSupplyBocked);
			}
		}

		if (Broodwar->self()->supplyTotal() < 400) {
			// update cushion factor
			cushionFactor.add(double(Broodwar->self()->minerals() + Broodwar->self()->gas()) / ((double)Broodwar->self()->supplyTotal() / 2));


			// check training buildings idle
			for (auto& building : Broodwar->self()->getUnits()) {
				UnitType unitType = building->getType();
				if (unitType.canProduce() && !unitType.isResourceDepot()) {
					if (building->isIdle() && trainingBuildingLastIdle[building] == 0) {
						trainingBuildingLastIdle[building] = Broodwar->getFrameCount();
					}
					if (!building->isIdle() && !trainingBuildingLastIdle[building] == 0) {
						buildingIdle.add(Broodwar->getFrameCount() - trainingBuildingLastIdle[building]);
						trainingBuildingLastIdle[building] = 0;
					}
				}
			}
		}

		// print macro stats
		Broodwar->drawTextScreen(5, 13 * 0, "MPM: %d avg: %.2f max: %.0f", rpmCounter.mpm(), avgMPM.getMean(), avgMPM.getMax());
		Broodwar->drawTextScreen(5, 13 * 1, "GPM: %d avg: %.2f max: %.0f", rpmCounter.gpm(), avgGPM.getMean(), avgGPM.getMax());
		Broodwar->drawTextScreen(5, 13 * 2, "Supply blocked avg: %.2f max: %.0f total: %.0f", supplyBlockedStat.getMean(), supplyBlockedStat.getMax(), supplyBlockedStat.getTotal());
		Broodwar->drawTextScreen(5, 13 * 3, "Cushion factor avg: %.2f max: %.0f", cushionFactor.getMean(), cushionFactor.getMax());
		Broodwar->drawTextScreen(5, 13 * 4, "Building idle avg: %.2f max: %.0f total: %.0f", buildingIdle.getMean(), buildingIdle.getMax(), buildingIdle.getTotal());
	}
}

void BenchmarkAI::onSendText(std::string text)
{
}

void BenchmarkAI::onReceiveText(BWAPI::Player player, std::string text)
{
}

void BenchmarkAI::onPlayerLeft(BWAPI::Player player)
{
}

void BenchmarkAI::onNukeDetect(BWAPI::Position target)
{
}

void BenchmarkAI::onUnitDiscover(BWAPI::Unit unit)
{
}

void BenchmarkAI::onUnitEvade(BWAPI::Unit unit)
{
}

void BenchmarkAI::onUnitShow(BWAPI::Unit unit)
{
// 	LOG(unit->getType() << " showed at frame: " << Broodwar->getFrameCount() << " time: " << Broodwar->elapsedTime());
	if (Broodwar->mapName() == "RC3" && zealotSpotted == 0 && unit->getType() == UnitTypes::Protoss_Zealot) {
		zealotSpotted = Broodwar->getFrameCount();
	}
}

void BenchmarkAI::onUnitHide(BWAPI::Unit unit)
{
}

void BenchmarkAI::onUnitCreate(BWAPI::Unit unit)
{
}

void BenchmarkAI::onUnitDestroy(BWAPI::Unit unit)
{
// 	LOG("Unit destroyed at frame " << Broodwar->getFrameCount());
	if (unit->getPlayer() == Broodwar->self())
		selfKilled++;
	else if (unit->getPlayer() == Broodwar->enemy())
		enemyKilled++;

	if (Broodwar->mapName() == "S2-D" && unit->getType() == UnitTypes::Terran_Refinery) {
		refineryDestroyed = Broodwar->getFrameCount();
	}
}

void BenchmarkAI::onUnitComplete(BWAPI::Unit unit)
{
}

void BenchmarkAI::onUnitMorph(BWAPI::Unit unit)
{
	if (Broodwar->mapName() == "S2-D" && unit->getType() == UnitTypes::Terran_Refinery) {
		if (refineryDestroyed > 0) refineryRestored = Broodwar->getFrameCount();
	}
}

void BenchmarkAI::onUnitRenegade(BWAPI::Unit unit)
{
}

void BenchmarkAI::onSaveGame(std::string gameName)
{}

bool BenchmarkModule::onAction(BWAPI::Tournament::ActionID actionType, void *parameter)
{
  switch ( actionType )
  {
  case Tournament::SendText:
  case Tournament::Printf:
    // Allow the AI module to send text
    return true;
  case Tournament::EnableFlag:
    switch ( *(int*)parameter )
    {
    case Flag::CompleteMapInformation:
//     case Flag::UserInput:
      // Disallow these two flags
      return false;
    }
    // Allow other flags if we add more that don't affect game play specifically
    return true;
  case Tournament::PauseGame:
  case Tournament::ResumeGame:
  case Tournament::SetFrameSkip:
  case Tournament::SetGUI:
  case Tournament::SetLocalSpeed:
  case Tournament::SetMap:
    return false; // Disallow these actions
  case Tournament::LeaveGame:
  case Tournament::SetLatCom:
  case Tournament::SetTextSize:
    return true; // Allow these actions
  case Tournament::SetCommandOptimizationLevel:
    return *(int*)parameter > MINIMUM_COMMAND_OPTIMIZATION; // Set a minimum command optimization level 
                                                            // to reduce APM with no action loss
  default:
    break;
  }
  return true;
}

void BenchmarkModule::onFirstAdvertisement()
{
  Broodwar->sendText("Welcome to StarCraft Benchmark!");
}

double BenchmarkAI::getTotalSqrtHP(BWAPI::Unitset units)
{
	double totalHP = 0;
	for (auto& unit : units) {
		totalHP += sqrt((double)(unit->getType().maxShields() + unit->getType().maxHitPoints()));
	}
	return totalHP;
}

double BenchmarkAI::getTimeToKill(BWAPI::Unitset unitsAttacking, BWAPI::Unitset unitsDefending)
{
	// get Hit Points of defending units
	double airHP = 0;
	double groundHP = 0;

	for (auto& unit : unitsDefending) {
		UnitType unitType = unit->getType();
		if (unitType.isFlyer())
			airHP += (double)(unitType.maxShields() + unitType.maxHitPoints());
		else
			groundHP += (double)(unitType.maxShields() + unitType.maxHitPoints());
	}

	// get Damage Per Frame of attacking units
	double airDPF = 0;
	double groundDPF = 0;
	double bothAirDPF = 0;
	double bothGroundDPF = 0;

	for (auto& unit : unitsAttacking) {
		UnitType unitType = unit->getType();
		if (unitType.airWeapon().damageAmount() > 0 && unitType.groundWeapon().damageAmount() > 0) {
			bothAirDPF += (double)unitType.airWeapon().damageAmount() / (double)unitType.airWeapon().damageCooldown();
			bothGroundDPF += (double)unitType.groundWeapon().damageAmount() / (double)unitType.groundWeapon().damageCooldown();
		} else {
			if (unitType.airWeapon().damageAmount() > 0)
				airDPF += (double)unitType.airWeapon().damageAmount() / (double)unitType.airWeapon().damageCooldown();
			if (unitType.groundWeapon().damageAmount() > 0)
				groundDPF += (double)unitType.groundWeapon().damageAmount() / (double)unitType.groundWeapon().damageCooldown();
			// In the case of Firebats and Zealots, the damage returned by BWAPI is not right, since they have two weapons:
			if (unitType == BWAPI::UnitTypes::Terran_Firebat || unitType == BWAPI::UnitTypes::Protoss_Zealot)
				groundDPF += (double)unitType.groundWeapon().damageAmount() / (double)unitType.groundWeapon().damageCooldown();
		}
	}

	// calculate time to kill
	double timeToKillAir = (airHP > 0) ? (airDPF == 0) ? INT_MAX : airHP / airDPF : 0;
	double timeToKillGround = (groundHP > 0) ? (groundDPF == 0) ? INT_MAX : groundHP / groundDPF : 0;
	if (bothAirDPF > 0) {
		if (timeToKillAir > timeToKillGround) {
			double combinetDPF = airDPF + bothAirDPF;
			timeToKillAir = (airHP > 0) ? (combinetDPF == 0) ? INT_MAX : airHP / combinetDPF : 0;
		} else {
			double combinetDPF = groundDPF + bothGroundDPF;
			timeToKillGround = (groundHP > 0) ? (combinetDPF == 0) ? INT_MAX : groundHP / combinetDPF : 0;
		}
	}

	return std::max(timeToKillAir, timeToKillGround);
}