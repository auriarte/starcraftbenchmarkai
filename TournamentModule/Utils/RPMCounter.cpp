#include "RPMCounter.h"

#include <cmath>

using namespace BWAPI;

void RPMCounter::init()
{
	botMPM = 0;
	botGPM = 0;
	botMPMCounter = 0;
	botGPMCounter = 0;
	lastUpdateMinerals = 0;
	lastUpdateGas = 0;
}

int RPMCounter::mpm() const
{
	return botMPM;
}

int RPMCounter::gpm() const
{
	return botGPM;
}

void RPMCounter::update()
{
	// update resources gathered since last time
	int currentMinreals = Broodwar->self()->gatheredMinerals() - 50; // we remove the initial 50
	botMPMCounter += currentMinreals - lastUpdateMinerals;
	lastUpdateMinerals = currentMinreals;

	int currentGas = Broodwar->self()->gatheredGas();
	botGPMCounter += currentGas - lastUpdateGas;
	lastUpdateGas = currentGas;

	// Note: formula from APMAlert
	const long double APMInterval = 0.95L;    // time after which actions are worth

	int currentFrame = Broodwar->getFrameCount();

	// Get the time difference between frames on fastest game speed (milliseconds).
	// That's numFrames * 42ms / frame .
	int timeDifference = (currentFrame - lastUpdateFrame) * 42;

	int totalTime = currentFrame * 42;

	// decay
	botMPMCounter = botMPMCounter * std::exp(-timeDifference / (APMInterval * 60000));
	botGPMCounter = botGPMCounter * std::exp(-timeDifference / (APMInterval * 60000));

	long double gameDurationFactor = 1.0L - std::exp(-totalTime / (APMInterval * 60000));
	if (gameDurationFactor < 1e-100L) gameDurationFactor = 1e-100L; //Prevent division by 0

	botMPM = static_cast<int>(botMPMCounter / (APMInterval*gameDurationFactor));
	botGPM = static_cast<int>(botGPMCounter / (APMInterval*gameDurationFactor));

	lastUpdateFrame = currentFrame;
}