#pragma once
#include <BWAPI.h>

// Keeps track of Minerals/Gas Per Minute (APM).
class RPMCounter
{
public:
	// Reset/initialize all values
	void init();

	// Retrieves the minerals per minute value
	int mpm() const;

	// Retrieves the gas per minute value
	int gpm() const;

	// Updates the APM counter
	void update();

private:
	int     botMPM;
	int     botGPM;
	long double  botMPMCounter;
	long double  botGPMCounter;

	int lastUpdateFrame;
	int lastUpdateMinerals;
	int lastUpdateGas;
};
