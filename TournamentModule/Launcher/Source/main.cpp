#include "stdafx.h"
#include "launcher.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	Launcher w;
	w.show();
	a.setWindowIcon(QIcon(":/Launcher/Resources/Nova.ico"));
	return a.exec();
}
