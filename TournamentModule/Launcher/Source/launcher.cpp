#include "stdafx.h"
#include "launcher.h"

std::string configPath("c:/StarCraft/bwapi-data/bwapi.ini");

Launcher::Launcher(QWidget *parent)
	: QMainWindow(parent)
{
	setupUi(this);

	// saving current working directory
	TCHAR pathBuffer[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, pathBuffer);
	currentPath = pathBuffer;

	// check ChaosLauncher path
	QByteArray text1 = chaosPathUI->text().toLocal8Bit();
	chaosLauncherPath = text1.data();
	if (!PathFileExists(chaosLauncherPath)) {
		printError(QString("Path doesn't exist: %1").arg((char*)chaosLauncherPath));
	}

	// Add check boxes for each map scenario
	std::string mapsFolder("c:\\StarCraft\\Maps\\BroodWar\\*.scx");
	WIN32_FIND_DATA FindFileData;
	QCheckBox *checkbox;

	HANDLE hFind = FindFirstFile((_bstr_t)mapsFolder.c_str(), &FindFileData);

	while (hFind != INVALID_HANDLE_VALUE) {
		QCheckBox *checkbox = new QCheckBox(QString::fromWCharArray(FindFileData.cFileName), this);
		scenariosLayout->addWidget(checkbox);
		mapsList.push_back(checkbox);

		if (!FindNextFile(hFind, &FindFileData)) {
			FindClose(hFind);
			hFind = INVALID_HANDLE_VALUE;
		}
	}

	// create timer
	iTimerGames = new QTimer(this);

	// signals/slots mechanism in action
	connect(runButton, SIGNAL(clicked()), this, SLOT(runBenchmark()));
	connect(iTimerGames, SIGNAL(timeout()), this, SLOT(checkGamesCompleted()));
}

Launcher::~Launcher()
{
	for (auto checkbox : mapsList) {
		delete checkbox;
	}
}

void Launcher::printError(const QString & text)
{
	// save    
	int fw = textEdit->fontWeight();
	QColor tc = textEdit->textColor();
	// append
	textEdit->setFontWeight(QFont::DemiBold);
	textEdit->setTextColor(QColor("red"));
	textEdit->append(text);
	// restore
	textEdit->setFontWeight(fw);
	textEdit->setTextColor(tc);
}

void Launcher::runBenchmark()
{
// 	textEdit->append(QString("Scenarios selected:"));
	
	// saving selected scenarios
	mapsSelected.clear();
	for (auto checkbox : mapsList) {
		if (checkbox->isChecked())  mapsSelected.push_back(checkbox->text().toStdString());
	}

	// set first map
	currentMapIndex = 0;
	std::string mapName(" Maps\\BroodWar\\");
	mapName += mapsSelected[currentMapIndex];
	WriteConfigString("auto_menu", "map", mapName.c_str());

	// start StarCraft
	launchStarcraft();

	// start timer to check games completed
	iTimerGames->start(2 * 1000); // in msec
}

void Launcher::checkGamesCompleted()
{
// 	textEdit->append(QString("checking games"));
	
	// count games on current map
	std::ifstream resultsFile("c:/StarCraft/bwapi-data/logs/Benchmark.txt");
	int lineNumber = 0;
	std::string line;
	while (std::getline(resultsFile, line)) {
		if (line.find(mapsSelected[currentMapIndex]) != std::string::npos) {
			lineNumber++;
		}
	}
	resultsFile.close();
// 	textEdit->append(QString("Map games: %1").arg(lineNumber));

	if (lineNumber >= numGames->text().toInt()) {
		// close StarCraft
		stopStarcraft();
		
		currentMapIndex++;
		if (currentMapIndex == mapsSelected.size()) {
			// if we finished process stats
			iTimerGames->stop();
			getStats();
		} else {
			// otherwise select new map
			std::string mapName(" Maps\\BroodWar\\");
			mapName += mapsSelected[currentMapIndex];
			WriteConfigString("auto_menu", "map", mapName.c_str());
			launchStarcraft();
		}
	}
}

void Launcher::getStats()
{
	std::map<std::string, Statistic> scores;
	std::map<std::string, Statistic> macroScores;
	std::ifstream resultsFile("c:/StarCraft/bwapi-data/logs/Benchmark.txt");
	std::string line, mapName;
	double score;
	std::vector<std::string> elems;
	while (std::getline(resultsFile, line)) {
		// split line
		std::stringstream ss(line);
		std::string item;
		elems.clear();
		while (std::getline(ss, item, ' ')) {
			elems.push_back(item);
		}
		mapName = elems[1];
		// get score
		if (mapName.find("S3") != std::string::npos) {
			// is a macro map
			if (macroScores["mpm"].getCount() <= numGames->text().toInt()) {
				double mpm = std::stod(elems[3].c_str());
				double gpm = std::stod(elems[4].c_str());
				double supplyBlocked = std::stod(elems[5].c_str());
				double cushionFactor = std::stod(elems[6].c_str());
				double buildingIdle = std::stod(elems[7].c_str());
				macroScores["mpm"].add(mpm);
				macroScores["gpm"].add(gpm);
				macroScores["supplyBlocked"].add(supplyBlocked);
				macroScores["cushionFactor"].add(cushionFactor);
				macroScores["buildingIdle"].add(buildingIdle);
			}
		} else {
			score = std::stod(elems[3].c_str());
			if (scores[mapName].getCount() <= numGames->text().toInt()) {
				scores[mapName].add(score);
			}
		}
	}
	resultsFile.close();

	textEdit->append(QString("Mean and StdDev by map"));
	for (auto& x : scores) {
		QString mean, stdDev;
		mean.sprintf("%0.4f", x.second.getMean());
		stdDev.sprintf("%0.4f", x.second.getStdDev());
		textEdit->append(QString("%1: %2 %3").arg(x.first.c_str()).arg(mean).arg(stdDev));
	}

	for (auto& x : macroScores) {
		QString mean, stdDev;
		mean.sprintf("%0.4f", x.second.getMean());
		stdDev.sprintf("%0.4f", x.second.getStdDev());
		textEdit->append(QString("%1: %2 %3").arg(x.first.c_str()).arg(mean).arg(stdDev));
	}
}

bool Launcher::launchStarcraft()
{
	// Start StarCraft through Chaosluncher 
	HWND chaosWindows = FindWindow(TEXT("TChaoslauncherForm"), NULL);
	if (!chaosWindows) {
		printError("Chaoslauncher not open (trying to start it)");
		// starting Chaoslauncher
		STARTUPINFOA si;
		PROCESS_INFORMATION pi;

		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		ZeroMemory(&pi, sizeof(pi));
		// change current working directory to Chaoslauncher path
		chdir(chaosLauncherPath);
		// create the process
		if (!CreateProcessA("Chaoslauncher.exe", NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, &si, &pi))
		{
			printError("Impossible to start Chaoslauncher");
			return FALSE;
		}
		// restore previous working directory
		chdir(currentPath);
		Sleep(500); // give time to lunch Chaos Launcher
		chaosWindows = FindWindow(TEXT("TChaoslauncherForm"), NULL);
	}
	// Chaoslauncher is running
	HWND chaosPanel = FindWindowEx(chaosWindows, 0, TEXT("TPanel"), NULL);
	if (!chaosPanel) {
		printError("Chaoslauncher Panel doesn't exist");
		return FALSE;
	} else {
		HWND ButtonHandle = FindWindowEx(chaosPanel, 0, TEXT("TButton"), TEXT("Start"));
		if (!ButtonHandle) {
			printError("Chaoslauncher Start button doesn't exist");
			return FALSE;
		} else {
			//SendMessage(ButtonHandle, BM_CLICK, 0, 0);
			SendMessageTimeout(ButtonHandle, BM_CLICK, 0, 0, SMTO_ABORTIFHUNG, 100, 0);
			Sleep(500); // give time to lunch Chaos Launcher
			// if using BWAPI 3.7.X we have a confirmation button
			HWND successWindows = FindWindow(NULL, TEXT("Success"));
			if (successWindows) {
				HWND ButtonHandle2 = FindWindowEx(successWindows, 0, TEXT("Button"), TEXT("OK"));
				if (ButtonHandle2) SendMessage(ButtonHandle2, BM_CLICK, 0, 0);
			}
			QDateTime dateTime = QDateTime::currentDateTime();
			QString dateTimeString = dateTime.toString();
			textEdit->append("StarCraft launched (" + dateTimeString + ")");
		}
	}

	return TRUE;
}

void Launcher::stopStarcraft()
{
	HWND starcraftWindows = FindWindow(NULL, TEXT("Brood War"));
	if (starcraftWindows) {
		DWORD starcraftProcessId = 0;
		GetWindowThreadProcessId(starcraftWindows, &starcraftProcessId);
		if (!starcraftProcessId) {
			printError("Error finding StarCraft process ID");
		} else {
			HANDLE starcraftProcess = OpenProcess(PROCESS_ALL_ACCESS, TRUE, starcraftProcessId);
			if (!starcraftProcess) {
				printError("Error opening StarCraft process");
			} else {
				TerminateProcess(starcraftProcess, 0);
				CloseHandle(starcraftProcess);
				textEdit->append("StarCraft process closed");
				Sleep(500); // give time to close StarCraft
			}
		}
	}
}